﻿using BinCalc.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace BinCalc.ViewModels
{
    public class MainViewModel : ObservableObject
    {
        public IRelayCommand<MathOperator> MathOperatorCommand { get; set; }
        public IRelayCommand ResultCommand { get; set; }
        public IRelayCommand ClearEntryCommand { get; set; }
        public IRelayCommand ClearCommand { get; set; }
        public IRelayCommand<string> InputNumberCommand { get; set; }

        private CalcMachine _calcMachine;

        private string _display;
        public string Display
        {
            get => _display;
            set
            {
                _display = value;
                OnPropertyChanged();
                NotifyCanExecuteChanged();
            }
        }
        
        public MainViewModel()
        {
            MathOperatorCommand = new RelayCommand<MathOperator>(ExecuteMathOperatorCommand, p => _calcMachine.State != State.Error);
            ResultCommand = new RelayCommand(ExecuteResultCommand, () => !(_calcMachine.State == State.Error || _calcMachine.State == State.Nop));
            ClearEntryCommand = new RelayCommand(ExecuteClearEntryCommand, () => !(_calcMachine.State == State.Nop || _calcMachine.State == State.Resolved || _calcMachine.State == State.Error));
            ClearCommand = new RelayCommand(ExecuteClearCommand, () => true);
            InputNumberCommand = new RelayCommand<string>(ExecuteInputNumberCommand, p => !(_calcMachine.State == State.Resolved || _calcMachine.State == State.Error));
            
            _calcMachine = new CalcMachine();
            Display = "0";
        }

        private void ExecuteMathOperatorCommand(MathOperator op)
        {
            _calcMachine.Operator = op;
            
            if (_calcMachine.State == State.ReadyForCalc)
            {
                _calcMachine.B = Display;
                Display = _calcMachine.Resolve();
            }
            else
            {
                _calcMachine.A = Display;
            }

            if (_calcMachine.State != State.Error)
            {
                _calcMachine.State = State.WaitsForSecondOperand;
            }

            NotifyCanExecuteChanged();
        }
        
        private void ExecuteResultCommand()
        {
            if (!(_calcMachine.State == State.CanRepeatLastCalc || _calcMachine.State == State.Resolved))
            {
                _calcMachine.B = Display;
            }

            _calcMachine.State = State.ReadyForCalc;
            Display = _calcMachine.Resolve();
        }

        private void ExecuteClearEntryCommand()
        {
            if (_calcMachine.State == State.ReadyForCalc)
            {
                Display = "0";
            }
        }

        private void ExecuteClearCommand()
        {
            Display = "0";
            _calcMachine = new CalcMachine();
            NotifyCanExecuteChanged();
        }

        private void ExecuteInputNumberCommand(string num)
        {
            if (Display == "0" || _calcMachine.State == State.WaitsForSecondOperand)
            {
                Display = "";
            }

            if (_calcMachine.State == State.WaitsForSecondOperand)
            {
                _calcMachine.State = State.ReadyForCalc;
            }
            
            Display += num;
        }

        private void NotifyCanExecuteChanged()
        {
            InputNumberCommand.NotifyCanExecuteChanged();
            ClearEntryCommand.NotifyCanExecuteChanged();
            ResultCommand.NotifyCanExecuteChanged();
            MathOperatorCommand.NotifyCanExecuteChanged();
        }
    }
}
