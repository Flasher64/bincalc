﻿using BinCalc.Helpers;

namespace BinCalc.Services
{
    public class CalcMachine
    {
        public MathOperator Operator { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public State State { get; set; }

        public CalcMachine()
        {
            Operator = MathOperator.Nop;
            A = "0";
            B = "0";
            State = State.Nop;
        }

        public string Resolve()
        {
            if (State < State.CanRepeatLastCalc)
            {
                return "0";
            }

            var result = 0;
            var a = BinaryConvertHelper.BinStringToDecNumber(A);
            var b = BinaryConvertHelper.BinStringToDecNumber(B);

            State = State.Resolved;
            switch (Operator)
            {
                case MathOperator.Add:
                    result = a + b;
                    break;
                case MathOperator.Sub:
                    result = a - b;
                    break;
            }

            if (!ValidateAllowedResult(result))
            {
                State = State.Error;
                return "ERROR";
            }

            var binaryResult = BinaryConvertHelper.DecToBin(result);
            A = binaryResult;
            return binaryResult;
        }

        private bool ValidateAllowedResult(int r)
        {
            return r >= 0;
        }
    }
    
    public enum MathOperator { Nop, Add, Sub }
    public enum State { Nop, WaitsForSecondOperand, CanRepeatLastCalc, ReadyForCalc, Resolved, Error }
}
