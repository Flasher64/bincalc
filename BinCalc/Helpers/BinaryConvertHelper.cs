﻿using System;

namespace BinCalc.Helpers
{
    public static class BinaryConvertHelper
    {
        public static string DecToBin(int value)
        {
            return Convert.ToString(value, 2);
        }

        public static int BinStringToDecNumber(string value)
        {
            try
            {
                return Convert.ToInt32(value, 2);
            }
            catch
            {
                return 0;
            }
        }
    }
}
